<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parking_lot_id')->unsigned();
            $table->boolean('is_fixed')->default(false);
            $table->integer('first_rate')->unsigned();
            $table->integer('first_rate_duration')->unsigned()->nullable();
            $table->integer('subsequent_rate')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_rates');
    }
}
