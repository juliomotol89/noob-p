<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_lots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('address');
            $table->enum('city', [
                'Manila',       'Caloocan',     'Pasay',
                'Quezon',       'Las Piñas',    'Makati',
                'Malabon',      'Mandaluyong',  'Marikina',
                'Muntinlupa',   'Navotas',      'Parañaque',
                'Pasig',        'Valenzuela',   'San Juan',
                'Taguig',
            ]);
            $table->integer('car_capacity')->unsigned()->nullable();
            $table->integer('motor_capacity')->unsigned()->nullable();
            $table->boolean('is_24hrs');
            $table->time('opening_time')->nullable();
            $table->time('closing_time')->nullable();
            $table->integer('car_occupied')->unsigned()->default(0);
            $table->boolean('is_parking_cars')->default(false);
            $table->integer('motor_occupied')->unsigned()->default(0);
            $table->boolean('is_parking_motors')->default(false);
            $table->integer('car_rates_id')->unsigned()->nullable();
            $table->integer('motor_rates_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_lots');
    }
}
