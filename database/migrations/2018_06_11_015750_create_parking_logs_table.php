<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rfid_tag');
            $table->integer('parking_lot_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->enum('vehicle_type', ['Car', 'Motorcycle']);
            $table->string('vehicle_make')->nullable();
            $table->string('vehicle_model')->nullable();
            $table->string('vehicle_color')->nullable();
            $table->string('plate_no')->nullable();
            $table->dateTime('time_in');
            $table->dateTime('time_out');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_logs');
    }
}
