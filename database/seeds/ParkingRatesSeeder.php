<?php

use Illuminate\Database\Seeder;

class ParkingRatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $parking_lot_id = [
            '1',
            '1',
            '2',
        ];
        $is_fixed = [
            false,
            true,
            false,
        ];
        $first_rate_duration = [
            3,
            null,
            3,
        ];
        $subsequent_rate = [
            20,
            null,
            20,
        ];
        for($i = 0 ; $i < count($parking_lot_id); $i++){
            DB::table('parking_rates')->insert(array(
                'id'                    => $i + 1,
                'parking_lot_id'        => $parking_lot_id[$i],
                'is_fixed'              => $is_fixed[$i],
                'first_rate'            => 50,
                'first_rate_duration'   => $first_rate_duration[$i],
                'subsequent_rate'       => $subsequent_rate[$i],
            ));
        }
    }
}
