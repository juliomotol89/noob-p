<?php

use Illuminate\Database\Seeder;

class ParkingLotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $title = [
            'West Tektite',
            'Valero',
        ];
        $slug = [
            'west_tektite',
            'valero',
        ];
        $address = [
            'San Antonio',
            'Belair',
        ];
        $city = [
            'Pasig',
            'Makati',
        ];
        $car_capacity = [
            30,
            40,
        ];
        $motor_capacity = [
            50,
            null,
        ];
        $is_parking_motors = [
            true,
            false,
        ];
        $car_rates_id = [
            1,
            3,
        ];
        $motor_rates_id = [
            2,
            null,
        ];
        for($i = 0 ; $i < count($title); $i++){
            DB::table('parking_lots')->insert(array(
                'id'                => $i + 1,
                'title'             => $title[$i],
                'slug'              => $slug[$i],
                'address'           => $address[$i],
                'city'              => $city[$i],
                'car_capacity'      => $car_capacity[$i],
                'motor_capacity'    => $motor_capacity[$i],
                'is_24hrs'          => true,
                'is_parking_cars'   => true,
                'is_parking_motors' => $is_parking_motors[$i],
                'car_rates_id'      => $car_rates_id[$i],
                'motor_rates_id'    => $motor_rates_id[$i],
            ));
        }
    }
}
