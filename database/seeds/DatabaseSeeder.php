<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    { 
        $this->call(ParkingLotSeeder::class);
        $this->call(ParkingRatesSeeder::class);
        $this->call(UserSeeder::class);
    }
}
