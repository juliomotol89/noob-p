<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('users')->insert(array(
            'id'            => 1,
            'first_name'    => 'Julio', 
            'last_name'     => 'Motol', 
            'email'         => 'julio@motol.com', 
            'password'      => bcrypt('Julio89*'),
        ));
    }
}
