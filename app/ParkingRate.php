<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingRate extends Model{
    protected $fillable = [
        'parking_lot_id',
        'is_fixed',
        'first_rate',
        'first_rate_duration',
        'subsequent_rate',
    ];
}
