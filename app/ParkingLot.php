<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ParkingRate;

class ParkingLot extends Model{
    protected $fillable = [
        'title',
        'slug',
        'city',
        'address',
        'car_capacity',
        'motor_capacity',
        'is_24hrs',
        'opening_time',
        'closing_time',
        'car_occupied',
        'is_parking_cars',
        'motor_occupied',
        'is_parking_motors',
        'car_rates_id',
        'motor_rates_id',
    ];

    public function carRates(){
        return $this->belongsTo('App\ParkingRate', 'car_rates_id');
    }
    public function motorRates(){
        return $this->belongsTo('App\ParkingRate', 'motor_rates_id');
    }

    public static function getParkingLots($options = []){
        if(isset($options['column'])){
            $raw_data = self::whereIn($options['column'], $options['targets'])->with(['carRates', 'motorRates'])->get();
            $formatted_data = self::convertToArray($raw_data, $options['details']);
        }else{
            $raw_data = self::with(['carRates', 'motorRates'])->get();
            $formatted_data = self::convertToArray($raw_data, $options['details']);
        }
        return $formatted_data;
    }
    private static function convertToArray($raw_data, $details){
        $formatted_data = [];
        if(!$raw_data){
            return 'N/A';
        }
        foreach($raw_data as $data){
            $new_data = self::format($data, $details);
            array_push($formatted_data, $new_data);
        }
        return $formatted_data;
    }
    private static function format($data, $details){
        $new_data['title'] = $data->title;
        $new_data['address'] = $data->address.', '.$data->city;
        $new_data['car_slots'] = $data->is_parking_cars ? $data->car_occupied.'/'.$data->car_capacity : 'N/A';
        $new_data['motor_slots'] = $data->is_parking_motors ? $data->motor_occupied.'/'.$data->motor_capacity : 'N/A';
        if($details == 'long'){
            $new_data['time_open'] = $data->is_24hrs ? 'Open 24hrs' : 'Open from '.$data->opening_time.' to '.$data->closing_time;
            if($data->is_parking_cars){
                $data->carRates->is_fixed ? 
                    $car_rates['fixed_rate'] = $data->carRates->first_rate : 
                    $car_rates['first_rate'] = $data->carRates->first_rate;
                isset($car_rates['first_rate']) ? $car_rates['first_rate_duration'] = $data->carRates->first_rate_duration : null ;
                isset($car_rates['first_rate']) ? $car_rates['succeding_hours'] = $data->carRates->subsequent_rate : null ;
            }
            if($data->is_parking_motors){
                $data->motorRates->is_fixed ? 
                    $motor_rates['fixed_rate'] = $data->motorRates->first_rate : 
                    $motor_rates['first_rate'] = $data->motorRates->first_rate;
                isset($motor_rates['first_rate']) ? $motor_rates['first_rate_duration'] = $data->motorRates->first_rate_duration : null ;
                isset($motor_rates['first_rate']) ? $motor_rates['succeding_hours'] = $data->motorRates->subsequent_rate : null ;
            }
            $new_data['rates'] = [
                'cars' => $data->is_parking_cars ? $car_rates : 'N/A',
                'motors' => $data->is_parking_motors ? $motor_rates : 'N/A',
            ];
        }
        return $new_data;
    }
}
