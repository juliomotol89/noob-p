<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Favorite;
use App\ParkingLot;
use Auth;

class FavoritesController extends Controller{
    public function view(){
        try{
            return response()->json([
                'status' => 'success',
                'favorites' => Favorite::getUserFavorites(Auth::id()),
            ]);
        }catch(Exception $e){
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
    public function add($parking_lot_slug){
        try{
            $parking_lot = ParkingLot::select('id')->where('slug', $parking_lot_slug)->first();
            Favorite::create([
                'user_id' => Auth::id(),
                'parking_lot_id' => $parking_lot->id
            ]);
            return response()->json([
                'status' => 'success'
            ]);
        }catch(Exception $e){
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
    public function remove($parking_lot_slug){
        try{
            $parking_lot = ParkingLot::select('id')->where('slug', $parking_lot_slug)->first();
            Favorite::where([
                ['user_id', '=', Auth::id()],
                ['parking_lot_id', '=', $parking_lot->id]
            ])->delete();
            return response()->json([
                'status' => 'success'
            ]);
        }catch(Exception $e){
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
