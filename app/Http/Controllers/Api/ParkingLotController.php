<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ParkingLot;

class ParkingLotController extends Controller{
    public function view($details = 'small', $parking_lot_slug = null){
        try{
            $options = $parking_lot_slug ? [
                'details'   => $details,
                'column'    => 'slug',
                'targets'   => [$parking_lot_slug],
            ] : [
                'details'   => $details,
            ]; 
            return response()->json([
                'status' => 'success',
                'parking_lots' => ParkingLot::getParkingLots($options)
            ]);
        }catch(Exception $e){
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
