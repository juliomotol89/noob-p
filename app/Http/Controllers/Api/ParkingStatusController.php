<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ParkingLog;
use Auth;

class ParkingStatusController extends Controller{
    public function view(){
        try{
            return response()->json([
                'status' => 'success',
                'parking_status' => ParkingLog::getUserLog(Auth::id())
            ]);
        }catch(Exception $e){
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
    public function add($rfid_tag){
        try{
            $parking_log = ParkingLog::where('rfid_tag', $rfid_tag)->first();
            $parking_log->user_id = Auth::id();
            $parking_log->save();
            return response()->json([
                'status' => 'success'
            ]);
        }catch(Exception $e){
            return response()->json([
                'status' => 'error'
            ]);
        }
    }
}
