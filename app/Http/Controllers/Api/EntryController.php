<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ParkingLog;

class EntryController extends Controller{
    public function newEntry(Request $request){
        $data = $request->all();
        ParkingLog::create([
            'rfid_tag' => $data['rfid_tag'],
            'parking_lot_id' => $data['parking_lot_id'],
            'vehilce_type' => $data['vehicle_type'],
        ]);
        return response()->json([
            'message' => 'Entry Added',
        ]);
    }
}
