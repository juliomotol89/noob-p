<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Route;
use Auth;
use App\User;

class AuthController extends Controller{
    public function login(Request $request){
        $data = $request->all();
    	$validator = Validator::make($data, [
            'client_id'     => 'required',
            'client_secret' => 'required',
            'email'         => 'required|email',
            'password'      => 'required'
        ]);

        if($validator->fails()){
            return response()->json([ 'error' => $validator->messages()->first() ], 401);
        }

        $params = [
    		'grant_type' 	=> 'password',
    		'client_id' 	=> $data['client_id'],
    		'client_secret' => $data['client_secret'],
			'scope' 		=> '',
			'username' 		=> $data['email'],
			'password'   	=> $data['password']
		];

        $request->request->add($params);
        $proxy = Request::create('oauth/token', 'POST');
        
    	return Route::dispatch($proxy);
    }

    public function refresh(Request $request){
    	$data = $request->all();
    	$validator = Validator::make($data, [
            'client_id'     => 'required',
            'client_secret' => 'required',
            'refresh_token'         => 'required'
        ]);
        
        $params = [
    		'grant_type' 	=> 'refresh_token',
    		'client_id' 	=> $data['client_id'],
    		'client_secret' => $data['client_secret'],    		
            'scope' 		=> '',
            'refresh_token' => $data['refresh_token']
		];

        $request->request->add($params);
        $proxy = Request::create('oauth/token', 'POST');
        
    	return Route::dispatch($proxy);
    }
}
