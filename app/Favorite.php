<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ParkingLot;

class Favorite extends Model{
    protected $fillable = [
        'user_id',
        'parking_lot_id',
    ];

    public static function getUserFavorites($user_id){
        $option = [];
        $targets = self::select('parking_lot_id')->where('user_id', $user_id)->get();
        foreach($targets as $target){
            $option[] = $target->parking_lot_id;
        }
        return ParkingLot::getParkingLots([
            'details'   => 'small',
            'column'    => 'id',
            'targets'   => $option,
        ]);
    }
}
