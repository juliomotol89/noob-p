<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ParkingLot;
use Carbon\Carbon;

class ParkingLog extends Model{
    const CREATED_AT = 'time_in';
    const UPDATED_AT = 'time_out';

    protected $fillable = [
        'rfid_tag',
        'user_id',
        'parking_lot_id',
        'vehicle_type',
        'vehicle_make',
        'vehicle_model',
        'vehicle_color',
        'plate_no',
    ];

    public function parkingLot(){
        return $this->belongsTo('App\ParkingLot');
    }

    public static function getUserLog($user_id){
        return self::convertToArray(self::where('user_id', $user_id)->with('parkingLot')->first());
    }
    private static function convertToArray($raw_data){
        if(!$raw_data){
            return 'N/A';
        }
        $raw_data->parkingLot->carRates();
        $raw_data->parkingLot->motorRates();
        $rate = $raw_data->vehicle_type == 'Car' ? $raw_data->parkingLot->carRates : $raw_data->parkingLot->motorRates;
        
        $time_in = Carbon::parse($raw_data->time_in);
        $parking_hours = $time_in->diffInHours(Carbon::now());

        $payment = $rate->first_rate;
        
        while($parking_hours - $rate->first_rate_duration > 0){
            $payment += $rate->subsequent_rate;
            $parking_hours--;
        }

        return [
            'title'         => $raw_data->parkingLot->title,
            'address'       => $raw_data->parkingLot->address.', '.$raw_data->parkingLot->city, 
            'vehicle_type'  => $raw_data->vehicle_type,
            'time_in'       => $raw_data->time_in,
            'payment_total' => $payment,
        ];
    } 
}
