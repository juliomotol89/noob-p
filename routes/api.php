<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('gate/entry', 'Api\EntryController@newEntry');
Route::post('login', 'AuthController@login');

Route::middleware('auth:api')->group(function(){
    Route::prefix('mobile')->group(function(){
        Route::get('parking-lots/{details?}/{parking_lot_slug?}', 'Api\ParkingLotController@view');
        Route::prefix('favorites')->group(function(){
            Route::get('/', 'Api\FavoritesController@view');
            Route::get('add/{parking_lot_slug}', 'Api\FavoritesController@add');
            Route::get('remove/{parking_lot_slug}', 'Api\FavoritesController@remove');
        });
        Route::prefix('parking-status')->group(function(){
            Route::get('/', 'Api\ParkingStatusController@view');
            Route::get('add/{rfid_tag}', 'Api\ParkingStatusController@add');
        });
    });
});